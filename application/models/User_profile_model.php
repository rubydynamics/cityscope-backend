<?php
class User_Profile_model extends CI_Model
{
	public function __construct()
  {
  	// Call the CI_Model constructor
    parent::__construct();
  }
	// add user profile details to user_profile table
	public function add_user_profile()
	{
		$data = array(
    	'first_name'=>$this->input->post('first-name'),
    	'last_name'=>$this->input->post('last-name'),
    	'mobile'=>$this->input->post('mobile'),
			'uid'=>$this->input->post('uid')
  	);
  	$this->db->insert('user_profile',$data);
	}

	public function check_for_profile($uid)
	{
		// boolean to check if profile does not exist
		$is_profile_there = false;
		//get uid
		$this->db->where('uid',$uid);
		//get user_profile table
		$query = $this->db->get('user_profile');

		// if number of rows returned is 0 then return true
		if($query->num_rows() == 0)
		{
			return $is_profile_there;
		}
	}
}
