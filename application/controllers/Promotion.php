<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class Promotion extends CI_Controller {

    function __construct()
    {
      parent::__construct();
      // load User_model
      $this->load->model('banner_model');
      $this->load->helper('form');
      $this->load->helper('url');
      // load form validation helper
      $this->load->library('form_validation');
    }

    public function show_banner($page='view_banner')
    {
      $data['title']= 'Add Promotion';
      $this->load->view('templates/admin/admin_header'); // Pass 'login' title to the Page Title
      $this->load->view('templates/admin/admin_side_bar');
      $this->load->view('pages/'.$page,$data);
      $this->load->view('templates/admin/admin_footer');
    }

    public function post()
    {
      //load form validation lib
      $this->load->library('form_validation');
      //error delimeter
      $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
      //field name, error message , validation rules
      $this->form_validation->set_rules('upload_image','Upload Image','required');
      $this->form_validation->set_rules('promotion-title','Promotion Title','required');
      $this->form_validation->set_rules('promotion-description','Promotion Description','required');
      $this->form_validation->set_rules('promotion-message','Promotion Message','required');
    }
  }

?>
