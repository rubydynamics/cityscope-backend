<div class="content">
  <div class="jumbotron">
    <h2><?php echo $title; ?></h2>

    <div class="alert alert-success">
      <p>Awesome! Your registration was successful!</p>
    </div>
    <p>You will be redirected shortly!</p>
    <script>
      var redirect = function() {
        window.location = "user";
      };

      //redirect to login page after 5 seconds
      setTimeout(redirect, 5000);
    </script>
  </div>
</div><!--<div class="content">-->
