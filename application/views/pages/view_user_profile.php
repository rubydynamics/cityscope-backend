<div class="col-lg-9">

<?php echo validation_errors(); ?>

<div id="user-profile-form">
	<h1><?php echo $title; ?></h1>
	<div class="alert alert-info">Complete your profile</div>
  <?php echo form_open('user_profile/user_profile_form'); ?>
  <!--
  <div class="form-group">
    <label for="fileToUpload">Select image to upload:</label>
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload Image" name="upload">
  </div>
  -->
    <div class="form-group">
        <label for="first-name">First Name:</label>
        <input type="text" size="20" id="first-name" name="first-name" class="form-control" required/>
    </div>
   
    <div class="form-group">
        <label for="last-name">Last Name:</label>
        <input type="text" size="20" id="last-name" name="last-name" class="form-control" required/>
    </div>

    <div class="form-group">
        <label for="mobile">Mobile Number:</label>
        <input type="number" size="20" id="mobile" name="mobile" class="form-control" required/>
    </div>

	<!--hidden field to hold session variable for user id; this is passed to the user profile table after submission-->
	<div class="form-group">
     <input type="hidden" size="20" id="uid" name="uid" class="form-control" value="<?php echo $this->session->userdata('uid'); ?>"/>
  </div>

	<input class="btn btn-primary" type="submit" value="Continue"/>
	<?php echo form_close(); ?>

</div><!--end of profile form-->

</div>
