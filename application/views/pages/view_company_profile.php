<?php echo validation_errors(); ?>
<div class="col-lg-9">
<div id="company-profile-form">
  <h1><?php echo $title; ?></h1>
  <?php echo form_open('company_profile/register'); ?>
  <div class="col-lg-6">
    <div><?php echo $reg_message; ?></div>
  <div class="form-group">
     <label for="company-name">Company Name:</label>
     <input type="text" size="20" id="company-name" name="company-name" class="form-control" required/>
  </div>

	<div class="form-group">
     <label for="country">Country:</label>
     <input type="text" size="20" id="country" name="country" class="form-control" required/>
  </div>

  <div class="form-group">
     <label for="city">City:</label>
     <input type="text" size="20" id="city" name="city" class="form-control" required/>
  </div>

	<div class="form-group">
     <label for="county">County:</label>
     <input type="text" size="20" id="county" name="county" class="form-control" required/>
  </div>

  <div class="form-group">
     <label for="address">Address:</label>
     <input type="text" size="20" id="address" name="address" class="form-control" required/>
  </div>

  <div class="form-group">
     <label for="zip">Zip:</label>
     <input type="number" size="20" id="zip" name="zip" class="form-control" required/>
  </div>
</div>
<div class="col-lg-6">
  <div class="form-group">
     <label for="phone">Phone:</label>
     <input type="number" size="20" id="phone" name="phone" class="form-control" required/>
  </div>

  <div class="form-group">
     <label for="email">Email:</label>
     <input type="email" size="20" id="email" name="email" class="form-control" required/>
  </div>

  <div class="form-group">
     <label for="website">Website:</label>
     <input type="url" size="20" id="website" name="website" class="form-control" required/>
  </div>

  <div class="form-group">
     <label for="categories">Categories:</label>
     <input type="text" size="20" id="categories" name="categories" class="form-control" required/>
  </div>

  <div class="form-group">
     <label for="facebook">Facebook:</label>
     <input type="url" size="20" id="facebook" name="facebook" class="form-control" required/>
  </div>

  <div class="form-group">
     <label for="twitter">Twitter:</label>
     <input type="url" size="20" id="twitter" name="twitter" class="form-control" required/>
  </div>
</div>
  <div class="form-group">
     <label for="location">Location:</label>
     <div id="company-map">

     </div>
     <div id="coordinates">
       <label>Latitude</label>
       <!-- <span id="coord-lat"></span> -->
        <input type="url" size="20" id="coord-lat" name="coord-lat" class="form-control" disabled="true"/>
       <label>Longitude</label>
       <!-- <span id="coord-lng"></span> -->
        <input type="url" size="20" id="coord-lng" name="coord-lng" class="form-control" disabled="true"/>
     </div>
  </div>

  <input id="submit-company" class="btn btn-primary" type="submit" value="Save" name="submit"/>
  <?php echo form_close(); ?>
</div>
</div>
