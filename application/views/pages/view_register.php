<?php echo validation_errors(); ?>
<div id="registration-form">
	<h1><?php echo $title; ?></h1>
  <?php echo form_open('user/register'); ?>

	<div class="form-group">
     <label for="username">Username:</label>
     <input type="text" size="20" id="username" name="username" class="form-control" required placeholder="Enter your username" value="<?php echo set_value('username'); ?>"/>
  </div>

	<div class="form-group">
     <label for="email">Email:</label>
     <input type="email" size="20" id="email" name="email" class="form-control" required placeholder="Enter a valid email address" value="<?php echo set_value('email'); ?>"/>
  </div>

	<div class="form-group">
     <label for="password">Password:</label>
     <input type="password" size="20" id="password" name="password" class="form-control" required placeholder="Enter password" value="<?php echo set_value('password'); ?>"/>
  </div>

	<div class="form-group">
     <label for="confirm-password">Confirm Password:</label>
     <input type="password" size="20" id="passconf" name="passconf" class="form-control" required placeholder="Confirm password" value="<?php echo set_value('passconf'); ?>"/>
  </div>

	<input class="btn btn-primary" type="submit" value="Submit"/>
	<?php echo form_close(); ?>

</div>
