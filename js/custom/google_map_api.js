//Google Map API for detecting shop location


var map;
var marker;

function initMap() {

    // map options
    var mapOptions = {
      scrollwheel: true,
      zoom: 14
    };

    // Create a map object and specify the DOM element for display.
    map = new google.maps.Map(document.getElementById('company-map'),mapOptions);

    // HTML 5 Geolocation
    // Does the browser support geolocation
    if(navigator.geolocation) {
      //yes it does support geolocation
      navigator.geolocation.getCurrentPosition(function(position){
        //geolocation coordinates
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };

        // marker options
        var markerOptions = {
          position: pos,
          map: map,
          title:'My Position!'
        };

        //save coordinates
        mapLat = pos.lat;
        mapLng = pos.lng;

        // set coordinate values to html elements
        document.getElementById('coord-lat').value = mapLat;
        document.getElementById('coord-lng').value = mapLng;
        //map marker
        marker = new google.maps.Marker(markerOptions);

        //set marker on map
        marker.setMap(map);

        // center map to geolocation position
        map.setCenter(pos);
      }, function() {handleLocationError(true);});
    }
    else {
      //Browser doesn't support geolocation
      handleLocationError(false);
    }
}

function handleLocationError(browserHasGeolocation) {
  browserHasGeolocation ? alert('Error: The Geolocation service failed.') : alert('Error: Your browser doesn\'t support geolocation.');
}
